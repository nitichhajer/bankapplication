package app;

import java.util.*;

abstract class Account implements BankingFunctionalities {

    private String accountID;
    public static HashMap<String,Float> accountDetails = new HashMap<String,Float>();
    public static HashMap<String,String> accountType = new HashMap<String,String>();

    public void setAccountDetails(String accountType, float balanceAmount) {
        //todo: implement using enum
        Set<String> validAccountsSet = new HashSet<String>(Arrays.asList("Savings", "Current"));

        //check if account type is valid i.e. exists in enum ValidAccountTypes
        if (!validAccountsSet.contains(accountType)) {
            throw new CustomException("Internal Error: Invalid Account Type");
        }

        Random rand = new Random();

        //generating unique id in range 100-999
        //do while accountID is not unique
        do {
            int accountNumber = rand.nextInt(900) + 100;
            accountID = accountType.charAt(0) + Integer.toString(accountNumber);        //appending account type identifier (eg. S for Savings Account)
        } while(accountDetails.containsKey(accountID));

        //setting details
        accountDetails.put(accountID, balanceAmount);
        this.accountType.put(accountID, accountType);

        System.out.println("Account Details" + accountDetails);

    }

    public String getAccountNumber () {
        return accountID;
    }

    public String getAccountType(String accountID) {
        return accountType.get(accountID);
    }

    public void setBalanceAmount(String accountID, float balanceAmount) {
        System.out.println("Balance updated for Account: " + accountID + " to " + balanceAmount);
        accountDetails.put(accountID, balanceAmount);
    }

    public float getBalanceAmount(String accountID) {
        return accountDetails.get(accountID);
    }

    public boolean checkAccountValidity(String accountID) {
        return accountDetails.containsKey(accountID);
    }


    public void deposit(String accountID, float depositAmount) {
        //checking if accountID is valid
        if(!checkAccountValidity(accountID)) {
            System.out.println("Invalid Account Number");
            return;
        }
        //current balance
        float balanceAmount = getBalanceAmount(accountID);
        //increment balance by amount deposited
        balanceAmount += depositAmount;
        this.setBalanceAmount(accountID, balanceAmount);
        System.out.println("Balance updated for Account: " + accountID + " to " + balanceAmount);
    }
    abstract public void withdraw(String accountID, float withdrawalAmount);
    abstract public void transfer(String fromAccountID, String toAccountID, float transferAmount);
}
