package app;

import java.lang.reflect.InvocationTargetException;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class BankApplication {

    Scanner sc = new Scanner (System.in);

    //enums for switch case menu
    private enum MenuChoice {
        CreateAccount, ManageAccount, ManageFunds, Exit
    }

    private enum ManageChoice {
        ViewDetails, UpdateDetails, PreviousMenu, Exit
    }

    private enum FundsChoice {
        Withdraw, Transfer, Deposit, PreviousMenu, Exit
    }

    public static void main (String args[]) {
        BankApplication bankObj = new BankApplication();
        bankObj.displayMenu();
    }

    //display initial menu
    public void displayMenu() {

        int choice = 0;
        MenuChoice menuChoice = null;


        while(true) {
            System.out.println("\n Enter: \n 1. Create Account \n 2. Manage Account \n 3. Manage Funds \n 4. Exit");
            try {
                choice = sc.nextInt();
                menuChoice = MenuChoice.values()[choice-1];

            }
            catch (InputMismatchException e) {
                System.out.println("Invalid Input");
                sc.next();
                displayMenu();
            }
            catch (ArrayIndexOutOfBoundsException e) {
                System.out.println("Invalid Input");
                displayMenu();
            }

            switch (menuChoice) {

                case CreateAccount: {
                    Person personObj = new Person();
                    personObj.createAccount();
                    break;
                }

                case ManageAccount: {
                    manageAccount();
                    break;
                }

                case ManageFunds: {
                    manageFunds();
                    break;
                }

                case Exit: {
                    System.exit(0);
                }
                default: {
                    System.out.println("Invalid Option");
                }
            }
        }
    }


    private String inputAccountID(String displayMessage) {
        String accountID;
        System.out.println(displayMessage);
        accountID = sc.next();
        return accountID;
    }

    public void manageAccount() {
        int choice = 0;
        ManageChoice manageChoice = null;

        System.out.println("Enter: \n 1. View Details \n 2. Update Details \n 3. Previous Menu \n 4. Exit ");
        try {
            choice = sc.nextInt();
            manageChoice = ManageChoice.values()[choice-1];
        }
        catch (InputMismatchException e) {
            System.out.println("Invalid Input");
            sc.next();
            manageAccount();
            displayMenu();
        }
        catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Invalid Input");
            manageAccount();
            displayMenu();
        }

        switch (manageChoice) {
            case ViewDetails: {
                viewDetails();
                break;
            }
            case UpdateDetails: {
                updateDetails();
                break;
            }

            case PreviousMenu: {
                break;
            }
            case Exit: {
                System.exit(0);
            }
            default: {
                System.out.println("Invalid Choice");
            }
        }
    }

    public void viewDetails() {
        //input accountID
        String accountID = inputAccountID("Enter Account ID: ");
        Person personObj = new Person();
        //get person details
        List<String> personDetails = personObj.getDetails(accountID);
        //if details is null, person doesn't exist i.e. invalid accountID
        if(personDetails == null) {
            System.out.println("Invalid Account Number");
            return;
        }
        //get balance amount
        float balanceAmount = Account.accountDetails.get(accountID);
        System.out.println("Person Details: " + personDetails + " Balance Amount:  "  + balanceAmount);
    }

    public void updateDetails() {
        String accountID = inputAccountID("Enter Account ID: ");
        Person personObj = new Person();

        //if person doesn't exist, return else input details and update
        if(!personObj.personExists(accountID)) {
            System.out.println("Invalid Account ID");
            return;
        }

        System.out.println("Enter Name, Age, Address");
        String name = sc.next();
        name += sc.nextLine();
        String age = sc.nextLine();
        String address = sc.nextLine();
        try {
            Integer.parseInt(age);
        }
        catch (NumberFormatException e) {
            System.out.println("Invalid Age");
            updateDetails();
            displayMenu();
        }
        personObj.setDetails(accountID, name, age, address);
    }

    //function to create object of account type corresponding to accountID and return it
    public Object getAccountTypeObject(String accountID) {
        String accountType = Account.accountType.get(accountID);
        //if accountType is null => accountID is invalid
        if(accountType == null) {
            return null;
        }
        String className = "app." + accountType + "Account";
        Object accountObj;
        //create object of type className and return it
        try {
            accountObj = Class.forName(className).getConstructor().newInstance();
            return accountObj;
        }
        catch (ClassNotFoundException | InvocationTargetException | InstantiationException | IllegalAccessException | NoSuchMethodException exceptionDescription) {
            System.out.println("Internal Error: Invalid Class/Method :" + exceptionDescription);
            displayMenu();
        }
        return null;
    }

    public void manageFunds() {
        int choice = 0;
        FundsChoice fundsChoice = null;

        System.out.println("Enter: \n 1. Withdraw  \n 2. Transfer \n 3. Deposit \n 4. Previous Menu \n 5. Exit");
        try {
            choice = sc.nextInt();
            fundsChoice = FundsChoice.values()[choice-1];

        }
        catch (InputMismatchException e) {
            System.out.println("Invalid Input");
            sc.next();
            manageFunds();
            displayMenu();
        }

        catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Invalid Input");
            manageFunds();
            displayMenu();
        }

        switch (fundsChoice) {
            case Withdraw: {
               withdrawAmount();
                break;
            }

            case Transfer: {
                transferAmount();
                break;
            }

            case Deposit: {
                depositAmount();
                break;
            }

            case PreviousMenu: {
                break;
            }
            case Exit: {
                System.exit(0);
            }
            default: {
                System.out.println("Invalid Option");
            }
        }
    }


    public void depositAmount() {
        //input details
        float depositAmount = 0;
        BankingFunctionalities accObj = null;

        String accountID = inputAccountID("Enter Account ID: ");
        System.out.println("Enter the amount to be deposited: ");
        try {
            depositAmount = sc.nextFloat();
        }
        catch (InputMismatchException e) {
            System.out.println("Invalid Input");
            sc.next();
            depositAmount();
            displayMenu();
        }

        //deposit amount
        try {
            accObj = (BankingFunctionalities) getAccountTypeObject(accountID);
        }
        catch(ClassCastException e) {
            System.out.println("Internal Error");
            displayMenu();
        }

        if (accObj == null) {
            System.out.println("Invalid Account Number");
        }

        else {
            accObj.deposit(accountID, depositAmount);
        }
    }

    public void withdrawAmount() {
        //input details
        float withdrawalAmount = 0;
        BankingFunctionalities accObj = null;
        String accountID = inputAccountID("Enter Account ID: ");
        System.out.println("Enter the amount to be withdrawn: ");
        try {
            withdrawalAmount = sc.nextFloat();
        }
        catch(InputMismatchException e) {
            System.out.println("Invalid Input");
            sc.next();
            withdrawAmount();
            displayMenu();
        }
        //get object of account corresponding to accountId
        try {
            accObj = (BankingFunctionalities) getAccountTypeObject(accountID);
        }
        catch(ClassCastException e) {
            System.out.println("Internal Error");
            displayMenu();
        }

        if (accObj == null) {
            System.out.println("Invalid Account Number");
        }

        else {
            accObj.withdraw(accountID, withdrawalAmount);
        }
    }

    public void transferAmount() {
        //input details
        float transferAmount = 0;
        BankingFunctionalities fromAccObj = null;
        BankingFunctionalities toAccObj = null;
        String fromAccountID = inputAccountID("Enter your Account ID: ");
        String toAccountID = inputAccountID("Enter Account ID to which you want to transfer: ");
        System.out.println("Enter the amount to be transferred: ");
        try {
            transferAmount = sc.nextFloat();
        }
        catch (InputMismatchException e) {
            System.out.println("Invalid Input");
            sc.next();
            transferAmount();
            displayMenu();
        }
        //transfer amount
        try {
            fromAccObj = (BankingFunctionalities) getAccountTypeObject(fromAccountID);
            toAccObj = (BankingFunctionalities) getAccountTypeObject(toAccountID);
        }
        catch (ClassCastException e)
        {
            System.out.println("Internal Error");
            displayMenu();
        }

        //ensure both accounts exist
        if (fromAccObj == null) {
            System.out.println("Invalid Sender Account Number");
        }

        if (toAccObj == null) {
            System.out.println("Invalid Receiver Account Number");
        }

        //ensure neither account is a SavingsAccount
        else if ((toAccObj instanceof SavingsAccount) || (fromAccObj instanceof SavingsAccount)) {
            System.out.println("You cannot transfer to or from Savings Account");
        }

        //ensure both accounts are different
        else if (toAccountID.equals(fromAccountID)) {
            System.out.println("You cannot transfer to your own account");
        }

        else {
            toAccObj.transfer(fromAccountID, toAccountID, transferAmount);
        }
    }

}
