package app;

import java.util.*;

public class Person {

    private String name;
    private String age;
    private String address;
    private static HashMap<String, List<String>> personDetails = new HashMap<>();

    private Scanner sc = new Scanner(System.in);
    BankApplication bankObj = new BankApplication();

    //choices to display in menu
    private enum AccountChoice {
        SavingsAccount, CurrentAccount, PreviousMenu
    }

    //return account details corresponding to accountID
    public List<String> getDetails(String accountID) {
        return personDetails.get(accountID);
    }

    //set details
    public void setDetails(String accountID, String name, String age, String address) {
        personDetails.put(accountID, Arrays.asList(name, age, address));
    }

    //check if person with given accountID exists
    public boolean personExists(String accountID) {
        return personDetails.containsKey(accountID);
    }

    public float inputDetails() {
        float depositAmount = 0;
        System.out.println("Enter initial deposit amount: ");
        try {
            depositAmount = sc.nextFloat();
            System.out.println("Enter Name, Age, Address: ");
            sc.nextLine();
            name = sc.nextLine();
            age = sc.nextLine();
            address = sc.nextLine();
            Integer.parseInt(age); //validating
        }
        catch (InputMismatchException e) {
            System.out.println("Invalid Input");
            sc.next();
            createAccount();
            bankObj.displayMenu();
        }

        catch (NumberFormatException e) {
            System.out.println("Invalid Age");
            createAccount();
            bankObj.displayMenu();
        }

        catch(RuntimeException e) {
            throw new CustomException(e);
        }

        return depositAmount;
    }

    public void printDetails(String accountNumber, float balanceAmount) {
        //store all details in Hashmap (accountID = [name, age, address])
        personDetails.put(accountNumber, Arrays.asList(name, age, address));

        //print details
        System.out.println("Person Details: " + personDetails);
        System.out.println("Your account  number is: " + accountNumber);
        System.out.println("Balance Amount: " + balanceAmount);
    }

    //todo: ensure accountType is valid
    public void createAccount() {
        int accChoice = 0;
        AccountChoice accountChoice = null;

        System.out.println("\n Enter: \n 1. Savings Account \n 2. Current Account \n 3. Previous Menu");
        try {
            accChoice = sc.nextInt();
            accountChoice = AccountChoice.values()[accChoice-1];                    //convert int to corresponding enum value
        }

        catch (InputMismatchException e)  {
            System.out.println("Invalid Input");
            sc.next();
            createAccount();
            bankObj.displayMenu();
        }
        catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Invalid Input");
            createAccount();
            bankObj.displayMenu();
        }

        switch (accountChoice) {
            case SavingsAccount: {
                //input data
                float depositAmount = inputDetails();
                //create savings account
                SavingsAccount sAccObj = new SavingsAccount();
                sAccObj.setAccountDetails("Savings", depositAmount);
                printDetails(sAccObj.getAccountNumber(), sAccObj.getBalanceAmount(sAccObj.getAccountNumber()));
                break;
            }

            case CurrentAccount: {
                //input data
                float depositAmount = inputDetails();
                //create current account
                CurrentAccount cAccObj = new CurrentAccount();
                cAccObj.setAccountDetails("Current", depositAmount);
                printDetails(cAccObj.getAccountNumber(), cAccObj.getBalanceAmount(cAccObj.getAccountNumber()));
                break;
            }

            case PreviousMenu: {
                break;
            }

            default: {
                System.out.println("Invalid Option");
            }
        }
    }
}
