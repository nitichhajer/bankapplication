package app;

public class SavingsAccount extends Account {
    private float withdrawalLimit = 100000;

    public void withdraw(String accountID, float withdrawalAmount) {

        //check if accountID is valid
        if(!checkAccountValidity(accountID)) {
            System.out.println("Invalid Account Number");
            return;
        }

        //get current balance amount
        float balanceAmount = getBalanceAmount(accountID);
        System.out.println("Balance Amount: " + balanceAmount);

        //ensure sufficient balance for withdrawal
        if(balanceAmount < withdrawalAmount) {
            System.out.println("Insufficient Funds");
        }

        //withdrawal amount shouldn't exceed limit
        else if (withdrawalAmount > withdrawalLimit) {
            System.out.println("Withdrawal Limit Exceeded");
        }
        else {
            balanceAmount -= withdrawalAmount;      //subtract withdrawal amount from balance
            this.setBalanceAmount(accountID, balanceAmount);
        }

    }

    public void transfer(String fromAccountID, String toAccountID, float transferAmount) {
        System.out.println("You cannot transfer from Savings Account");
    }
}

