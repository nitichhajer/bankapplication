package app;

public class CurrentAccount extends Account  {

    public void withdraw(String accountID, float withdrawalAmount) {

        //check if accountID is valid
        if(!checkAccountValidity(accountID)) {
            System.out.println("Invalid Account Number");
            return;
        }

        //get current balance amount
        float balanceAmount = getBalanceAmount(accountID);
        System.out.println("Balance Amount: " + balanceAmount);

        //ensure sufficient balance for withdrawal
        if(balanceAmount < withdrawalAmount) {
            System.out.println("Insufficient Funds");
        }

        else {
            balanceAmount -= withdrawalAmount;
            this.setBalanceAmount(accountID, balanceAmount);
        }

    }

    public void transfer(String fromAccountID, String toAccountID, float transferAmount) {

        //check validity for both account IDs
        if(!checkAccountValidity(fromAccountID)) {
            System.out.println("Invalid Sender Account Number");
            return;
        }
        if(!checkAccountValidity(toAccountID)) {
            System.out.println("Invalid Receiver Account Number");
            return;
        }

        //get balance of both accounts
        float fromBalanceAmount = getBalanceAmount(fromAccountID);
        float toBalanceAmount = getBalanceAmount(toAccountID);


        //ensure sufficient balance for withdrawal
        if(fromBalanceAmount < transferAmount) {
            System.out.println("Insufficient Funds");
        }
        else {
            fromBalanceAmount -= transferAmount;        //subtract transfer amount from sender's balance
            toBalanceAmount += transferAmount;          //add transfer amount to receiver's balance
            this.setBalanceAmount(fromAccountID, fromBalanceAmount);
            this.setBalanceAmount(toAccountID, toBalanceAmount);
        }
    }
}
