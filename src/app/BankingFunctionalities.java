package app;

public interface BankingFunctionalities {

    public void withdraw(String accountID, float withdrawalAmount);
    public void transfer(String fromAccountID, String toAccountID, float transferAmount);
    public void deposit(String accountID, float depositAmount);

}
